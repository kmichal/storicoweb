name := "storicoweb"

version := "1.0"

scalaVersion := "2.10.1"

EclipseKeys.withSource := true

EclipseKeys.createSrc := EclipseCreateSrc.Default + EclipseCreateSrc.Resource

resolvers += "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/releases"

resolvers += "Ansvia Releases Repo" at "http://scala.repo.ansvia.com/releases/"

scalacOptions ++= Seq("-unchecked", "-deprecation", "-feature")

libraryDependencies += "org.scalatest" % "scalatest_2.10" % "2.0.M5b" % "test"

libraryDependencies += "com.google.guava" % "guava" % "14.0-rc1"

libraryDependencies += "org.apache.commons" % "commons-lang3" % "3.1"

libraryDependencies += "com.google.code.findbugs" % "jsr305" % "2.0.1"

libraryDependencies += "com.propensive" % "rapture-io" % "0.7.2"

libraryDependencies += "com.ansvia.graph" % "blueprints-scala_2.10" % "0.1.0"

libraryDependencies += "org.slf4j" % "slf4j-api" % "1.7.3"

libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.0.10"

libraryDependencies += "com.typesafe" % "scalalogging-slf4j_2.10" % "1.0.1"

libraryDependencies += "com.github.nscala-time" %% "nscala-time" % "0.4.0"