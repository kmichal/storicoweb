package pl.edu.agh.ztis.storicoweb

import java.awt.Dimension
import javax.swing.JFrame
import javax.swing.JProgressBar
import javax.swing.SwingUtilities
import java.util.concurrent.atomic.AtomicInteger

class ProgressBar(lengthOfTask: Int, title: String = "Downloading...") extends JFrame(title) {

  private val progressBar = new JProgressBar(0, lengthOfTask)

  private val progress = new AtomicInteger(0)

  SwingUtilities.invokeLater(new Runnable() {
    def run() = createAndShowGUI()
  })

  def createAndShowGUI() {
    JFrame.setDefaultLookAndFeelDecorated(true)
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)
    setMinimumSize(new Dimension(300, 100))

    progressBar.setValue(0);
    progressBar.setStringPainted(true);
    add(progressBar)

    pack()
    setVisible(true)
  }

  def makeProgress() {
    val value = progress.incrementAndGet()
    progressBar.setValue(value)
  }

  def setValue(value: Int) {
    progress.set(value)
    progressBar.setValue(value)
  }

  def firstName_=(s: String) {}

  def close() {
    setVisible(false)
  }
}