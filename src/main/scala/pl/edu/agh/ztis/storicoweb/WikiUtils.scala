package pl.edu.agh.ztis.storicoweb

import scala.collection.mutable.HashMap
import scala.collection.mutable.MultiMap
import scala.collection.mutable.Set
import scala.xml.Elem
import scala.xml.XML

import com.typesafe.scalalogging.slf4j.Logging

import rapture.io.CharAccumulator
import rapture.io.FormPostType
import rapture.io.Http
import rapture.io.HttpResponseCharReader
import rapture.io.makeReadable

final object WikiConnector {

  private val wiki = Http / "en.wikipedia.org" / "w" / "api.php"

  private val wikiOptions = Map('action -> "query", 'format -> "xml")

  def queryXML(queryStrings: Map[Symbol, String]): Elem = {
    val rawXml = wiki.post(queryStrings ++ wikiOptions).slurp[Char]
    return XML.loadString(rawXml)
  }
}

object WikiUtils extends Logging {
  def peopleBornFromTill(startYear: Int, endYear: Int): MultiMap[Int, String] = {
    val yearPeople = new HashMap[Int, Set[String]]() with MultiMap[Int, String]

    for (year <- ((startYear to endYear).par)) yield {
      yearPeople(year) = WikiPeople(year)
    }

    return yearPeople
  }

  def getContinueCode(xml: Elem, continueCodeKey: String): Option[(Symbol, String)] = {
    return (xml \ "query-continue" \\ s"@$continueCodeKey").text match {
      case "" => None
      case continueCodeValue => Some(Symbol(continueCodeKey), continueCodeValue)
    }
  }
}

object WikiLink {

  private val linkPattern = """(\[\[.*?\]\])""".r

  private val clearPattern = """\[\[(.*?)(?:\|.*)?\]\]""".r

  def getAll(content: String): List[String] = {
    val linkIterator = for (linkPattern(rawLink) <- linkPattern findAllIn content) yield clearLink(rawLink) match {
      case Some(link) => link
      case None => rawLink
    }

    return linkIterator.toList
  }

  private def clearLink(rawLink: String): Option[String] = clearPattern findFirstIn rawLink match {
    case Some(clearPattern(link)) => Some(link)
    case None => None
  }
}