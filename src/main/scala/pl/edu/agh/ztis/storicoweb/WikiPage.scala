package pl.edu.agh.ztis.storicoweb

import scala.collection.mutable.HashSet
import scala.collection.mutable.Set
import scala.xml.Elem

case class WikiPage(title: String) {
  private lazy val _content = new WikiContent(title)

  lazy val content = _content.content

  lazy val links = _content.links

  lazy val pageLinks = WikiPageLinks(title)

  lazy val categories = WikiCategories(title)
}

class WikiContent(title: String) {

  private val query = Map('titles -> title, 'prop -> "revisions", 'rvprop -> "content")

  private val linkPattern = """(\[\[.*?\]\])""".r

  lazy val links = WikiLink.getAll(content)

  lazy val content: String = {
    val xml = WikiConnector.queryXML(query)
    (xml \\ "rev").text
  }
}

object WikiCategories {

  def apply(title: String): Set[String] = {
    val query = Map('titles -> title, 'prop -> "categories", 'cllimit -> "500")
    return new ContinuePage(query, "cl", "clcontinue").elements map (_.substring("Category:".length))
  }
}

object WikiPageLinks {

  def apply(title: String): Set[String] = {
    val query = Map('titles -> title, 'prop -> "links", 'pllimit -> "500")
    return new ContinuePage(query, "pl", "plcontinue").elements()
  }
}

object WikiPeople {

  def apply(bornYear: Int): Set[String] = {
    val yearRepresentation = if (bornYear <= 0) s"${-1 * bornYear}_BC" else bornYear.toString
    val query = Map('cmtitle -> s"Category:${yearRepresentation}_births", 'list -> "categorymembers", 'cmlimit -> "500")
    return new ContinuePage(query, "cm", "cmcontinue").elements()
  }
}

class ContinuePage(query: Map[Symbol, String], elementIdentifier: String, continueCode: String) {

  def elements(): Set[String] = {
    val xml = WikiConnector.queryXML(query)
    return elements(xml, new HashSet[String]())
  }

  private def elements(xml: Elem, acc: Set[String]): Set[String] = {
    for (person <- (xml \\ elementIdentifier \\ "@title")) {
      acc += person.text
    }

    return WikiUtils.getContinueCode(xml, continueCode) match {
      case None => acc
      case Some(continueCode) => {
        val nextXML = WikiConnector.queryXML(query + continueCode)
        elements(nextXML, acc)
      }
    }
  }
}