package pl.edu.agh.ztis.storicoweb

import java.io.FileOutputStream
import scala.collection.JavaConversions.asScalaBuffer
import scala.collection.JavaConversions.asScalaSet
import scala.collection.JavaConversions.seqAsJavaList
import scala.collection.mutable.HashMap
import scala.collection.parallel.ForkJoinTaskSupport
import scala.concurrent.forkjoin.ForkJoinPool
import com.ansvia.graph.BlueprintsWrapper.DbObject
import com.ansvia.graph.BlueprintsWrapper.edgeToPropertyAccessor
import com.ansvia.graph.BlueprintsWrapper.vertexToPropertyAccessor
import com.ansvia.graph.BlueprintsWrapper.vertexWrapper
import com.google.common.base.Throwables
import com.google.common.collect.HashMultiset
import com.tinkerpop.blueprints.Graph
import com.tinkerpop.blueprints.Vertex
import com.tinkerpop.blueprints.impls.tg.TinkerGraph
import com.tinkerpop.blueprints.util.io.gml.GMLWriter
import com.tinkerpop.blueprints.util.io.graphml.GraphMLWriter
import com.typesafe.scalalogging.slf4j.Logging
import scala.xml.XML

case class Person(label: String, bornYear: Int) extends DbObject

case class Category(label: String) extends DbObject

class WikiGraph(startDate: Int, endDate: Int)(implicit graph: Graph) extends Logging {

  private val peopleVertices = downloadPeopleVertices(10)

  private val categoryVertices = HashMap[String, Vertex]()

  private val people = peopleVertices.keySet

  connectVertices(10)

  private def downloadPeopleVertices(threadsNum: Int = 4): Map[String, Vertex] = {
    val parallelPeriod = threadsNum match {
      case 1 => startDate to endDate
      case threadsNum => {
        val parallel = (startDate to endDate).par
        parallel.tasksupport = new ForkJoinTaskSupport(new ForkJoinPool(threadsNum))
        parallel
      }
    }

    val peopleVertices = HashMap[String, Vertex]()

    val progressBar = new ProgressBar(endDate - startDate, "Downloading people (only names)...")
    for (bornYear <- parallelPeriod) {
      val peopleNames = WikiPeople(bornYear)
      peopleNames.foreach { personName => peopleVertices(personName) = Person(personName, bornYear).save }

      progressBar.makeProgress
    }
    progressBar.close

    logger.info(s"people: ${peopleVertices.size}")
    peopleVertices.toMap
  }

  private def connectVertices(threadsNum: Int = 4) {
    val parallelPeople = threadsNum match {
      case 1 => people
      case threadsNum => {
        val parallel = people.par
        parallel.tasksupport = new ForkJoinTaskSupport(new ForkJoinPool(threadsNum))
        parallel
      }
    }

    val progressBar = new ProgressBar(people.size, "Downloading articles... Connecting people...")
    try {
      for (personName <- parallelPeople) {
        val wikiPage = WikiPage(personName)
        val infobox = Infobox(wikiPage)
        try {
          connectByLinks(wikiPage)
          connectToCategories(wikiPage)
          connectByInfobox(infobox)
          addVertexProperties(infobox)
          addVertexArticleLenght(wikiPage)
        } catch {
          case e: Throwable => logger.error(s"$personName: ${Throwables.getCausalChain(e).mkString(", ")}")
        }

        progressBar.makeProgress
      }
    } catch {
      case e: Throwable => logger.error(Throwables.getCausalChain(e).mkString(", "))
    } finally {
      progressBar.close
    }
  }

  private def connectByLinks(wikiPage: WikiPage) {
    val currentVertex = peopleVertices(wikiPage.title)

    val linksToPeople = filterLinksByPeople(wikiPage.links)
    val linksToPeopleMultiset = HashMultiset.create(linksToPeople)

    for (linkToPerson <- linksToPeopleMultiset.entrySet()) {
      val otherVertex = peopleVertices(linkToPerson.getElement())
      val edge = currentVertex --> "link" --> otherVertex <

      edge.set("weight", linkToPerson.getCount())
    }
  }

  private def connectByInfobox(infobox: Infobox) {
    val currentVertex = peopleVertices(infobox.title)

    for ((properties, links) <- infobox.toWikiLinks; linkToPerson <- filterLinksByPeople(links)) {
      val otherVertex = peopleVertices(linkToPerson)
      currentVertex --> properties --> otherVertex

      if (properties.length > 50) {
        logger.warn(s"${infobox.title}: $properties")
      }
    }
  }

  private def filterLinksByPeople(links: List[String]) = links filter people

  private def addVertexProperties(infobox: Infobox) {
    val currentVertex = peopleVertices(infobox.title)

    infobox.birthDate match {
      case Some(date) => currentVertex.set("birth date", date.toDate())
      case None =>
    }

    infobox.deathDate match {
      case Some(date) => currentVertex.set("death date", date.toDate())
      case None =>
    }
  }

  private def addVertexArticleLenght(wikiPage: WikiPage) {
    val currentVertex = peopleVertices(wikiPage.title)
    currentVertex.set("article lenght", wikiPage.content.length)
  }

  private def connectToCategories(wikiPage: WikiPage) {
    val currentVertex = peopleVertices(wikiPage.title)

    for (category <- wikiPage.categories) {
      val otherVertex = categoryVertices.getOrElseUpdate(category, new Category(category).save)
      currentVertex --> "category" --> otherVertex
    }
  }

  def saveToGraphML(path: String) {
    val outFile = new FileOutputStream(path)
    GraphMLWriter.outputGraph(graph, outFile)
    outFile.close
  }

  def saveToGML(path: String) {
    val outFile = new FileOutputStream(path)
    GMLWriter.outputGraph(graph, outFile)
    outFile.close
  }
}

object Wikipedia extends App {
  implicit val graph = new TinkerGraph()

  val conf = XML.load(classOf[WikiGraph].getResourceAsStream("/config.xml"))

  val start = (conf \ "start" text) toInt
  val end = (conf \ "end" text) toInt
  val filename = conf \ "filename" text

  val wikiGraph = new WikiGraph(start, end)
  wikiGraph.saveToGraphML(s"${filename}.graphml")
  wikiGraph.saveToGML(s"${filename}.gml")
  graph.shutdown

  System.exit(0)
}

