package pl.edu.agh.ztis.storicoweb

import scala.language.postfixOps
import com.github.nscala_time.time.Imports.DateTime
import com.github.nscala_time.time.Imports.RichDateTime
import com.github.nscala_time.time.Imports.RichDateTimeProperty

case class Infobox(title: String, properties: Map[String, String]) {

  val datePattern = """(\d+?)\|(\d+?)\|(\d+?)\|""".r

  def toWikiLinks(): Map[String, List[String]] = {

    return properties map {
      case (key, value) => key -> WikiLink.getAll(value)
    } filterNot {
      case (_, links) => links.isEmpty
    } toMap
  }

  lazy val birthDate: Option[DateTime] = {
    properties get "birth_date" match {
      case Some(birthDate) => parseDate(birthDate)
      case None => None
    }
  }

  lazy val deathDate: Option[DateTime] = {
    properties get "death_date" match {
      case Some(deathDate) => parseDate(deathDate)
      case None => None
    }
  }

  private def parseDate(date: String): Option[DateTime] = {
    datePattern findFirstIn date match {
      case Some(datePattern(year, month, day)) => Some(new DateTime(0).withYear(year.toInt).month(month.toInt).day(day.toInt))
      case None => None
    }
  }
}

object Infobox {

  private val infoboxExtractPattern = """(?s)\{\{Infobox.*?\|\}\}""".r

  private val infoboxProperties = """\|(.*?)=(.*)""".r

  def apply(wikiPage: WikiPage): Infobox = create(wikiPage.title, wikiPage.content)

  private def create(title: String, content: String): Infobox = {
    val properties = infoboxExtractPattern findFirstIn content match {
      case Some(infoboxContent) => getProperties(infoboxContent)
      case None => Map[String, String]()
    }

    return Infobox(title, properties)
  }

  private def getProperties(infobox: String): Map[String, String] = {
    val properties = for (infoboxProperties(key, value) <- infoboxProperties findAllIn infobox) yield (key.trim.toLowerCase -> value.trim)
    return properties.toMap
  }
}