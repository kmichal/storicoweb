package pl.edu.agh.ztis.storicoweb

import org.scalatest.FunSpec
import org.scalatest.matchers.ShouldMatchers
import java.util.Calendar
import com.github.nscala_time.time.Imports._

class StoricoTests extends FunSpec with ShouldMatchers {

  val napoleon = new WikiPage("Napoleon")

  describe("A Napoleon's Wiki Page") {
    it("should have at least 1000 page links") {
      napoleon.pageLinks.size should be > (1000)
    }

    it("should have at least 600 links") {
      napoleon.links.size should be > (600)
    }

    it("should have at least 25 categories") {
      napoleon.categories.size should be > (25)
    }

    it("should contains keywords like Napoleon, Maria etc.") {
      napoleon.content should include("Napoleon")
      napoleon.content should include("Maria")
    }
  }

  describe("A Wiki Page") {
    it("should download page with spaces in title") {
      val julia = new WikiPage("Julia (daughter of Drusus the Younger)")
      julia.content should include("Julia")
    }
  }

  describe("A Wiki Utils") {
    it("should download list of people born from till") {
      val people = WikiUtils.peopleBornFromTill(1, 10)

      people.size should be > (0)
    }

    it("should download persons born BC") {
      val personsBothBC = WikiUtils.peopleBornFromTill(-10, -2)
      personsBothBC.size should be > (0)

      val personsBCAC = WikiUtils.peopleBornFromTill(-10, 10)
      personsBCAC.size should be > (personsBothBC.size)
    }
  }

  describe("A Napoleon's Infobox") {
    val infobox = Infobox(napoleon)

    it("should have at least 10 raw properties") {
      infobox.properties.size should be > (10)
    }
    it("should get accurate birth date") {
      infobox.birthDate.get should be === new DateTime(0).withYear(1769).month(8).day(15)
    }
    it("should get accurate death date") {
      infobox.deathDate.get should be === new DateTime(0).withYear(1821).month(5).day(5)
    }
  }
}